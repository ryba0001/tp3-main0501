using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class mouvement : MonoBehaviour
{

    private List<Camera> cameras;
    private int currentCameraIndex;

    // Start is called before the first frame update
    // Changer de caméra avec les touches P et O
    void Start()
    {
        cameras = new List<Camera>(FindObjectsOfType<Camera>());
        currentCameraIndex = 0;
        for (int i = 0; i < cameras.Count; i++)
        {
            if (i == currentCameraIndex)
            {
                cameras[i].enabled = true;
            }
            else
            {
                cameras[i].enabled = false;
            }
        }
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKey(KeyCode.DownArrow))
        {
            transform.Translate(Vector3.down * 0.005f);
        }
        if (Input.GetKey(KeyCode.UpArrow))
        {
            transform.Translate(Vector3.up * 0.009f);
            if (Input.GetKey(KeyCode.LeftShift))
            {
                transform.Translate(Vector3.up * 0.012f);
            }
        }
        if (Input.GetKey(KeyCode.LeftArrow))
        {
            transform.Rotate(Vector3.forward * -0.3f);
        }
        if (Input.GetKey(KeyCode.RightArrow))
        {
            transform.Rotate(Vector3.forward * 0.3f);
        }
        // Commande pour changer de caméra
        if (Input.GetKeyDown(KeyCode.P))
        {
            cameras[currentCameraIndex].enabled = false;
            currentCameraIndex++;
            if (currentCameraIndex == cameras.Count)
            {
                currentCameraIndex = 0;
            }
            cameras[currentCameraIndex].enabled = true;
        }
        if (Input.GetKeyDown(KeyCode.O))
        {
            cameras[currentCameraIndex].enabled = false;
            currentCameraIndex--;
            if (currentCameraIndex == -1)
            {
                currentCameraIndex = cameras.Count - 1;
            }
            cameras[currentCameraIndex].enabled = true;
        }
    }

    // Permet d'afficher la légende permettant une meilleure compréhension des commandes
    void OnGUI()
    {
        // Définir la position et la taille du rectangle
        Rect rect = new Rect(Screen.width - 210, Screen.height - 230, 200, 220);

        // Dessiner le rectangle
        GUI.Box(rect, "Contrôles de la grue");

        // Ajouter les labels de texte
        GUI.Label(new Rect(Screen.width - 200, Screen.height - 200, 180, 20), "Bouger : Flèche directionnelle");
        GUI.Label(new Rect(Screen.width - 200, Screen.height - 180, 180, 20), "Chariot : Z et S");
        GUI.Label(new Rect(Screen.width - 200, Screen.height - 160, 180, 20), "Lever/Baisser Flèche : W/X");
        GUI.Label(new Rect(Screen.width - 200, Screen.height - 140, 180, 20), "Tourner Flèche : C/V");
        GUI.Label(new Rect(Screen.width - 200, Screen.height - 120, 180, 20), "Grappin : T/G");
        GUI.Label(new Rect(Screen.width - 200, Screen.height - 100, 180, 20), "Déployer/replier pieds : A/E");
        GUI.Label(new Rect(Screen.width - 200, Screen.height - 80, 180, 20), "Poser pieds : R/F");
        GUI.Label(new Rect(Screen.width - 200, Screen.height - 60, 180, 20), "Changer de caméra : O/P");
        GUI.Label(new Rect(Screen.width - 200, Screen.height - 40, 180, 20), "Lâcher objet : Espace");


        // Chronomèter le temps de jeu
        // je veux que le temps soit en minutes secondes

        float time = Time.time;
        int minutes = Mathf.FloorToInt(time / 60F);
        int seconds = Mathf.FloorToInt(time - minutes * 60);
        string niceTime = string.Format("{0:0}:{1:00}", minutes, seconds);

        GUI.Label(new Rect(10, 10, 180, 20), "Temps de jeu : " + niceTime);
    }



}
