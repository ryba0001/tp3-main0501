using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//Commande de translation
//Pour déployer les chariots Positif : Z - Négatif : S
//Pour descendre et monter le crochet : Descendre : G - Monter : T

public class TranslationCommande : MonoBehaviour
{
    public GameObject Translation;
    public string axe = "Fleche";

    void Update() // Renvoie l'état de mouvement à TranslationControleur
    {
        float input = Input.GetAxis(axe);
        EtatTranslation moveState = MoveStateForInput(input);
        TranslationControleur controller = Translation.GetComponent<TranslationControleur>();
        controller.moveState = moveState;
    }

    // Renvoie l'état dans lequel devrait être l'articulation de mouvement
    EtatTranslation MoveStateForInput(float input)
    {
        if (input > 0)
        {
            return EtatTranslation.Positif;
        }
        else if (input < 0)
        {
            return EtatTranslation.Negatif;
        }
        else
        {
            return EtatTranslation.Fixe;
        }
    }
}
