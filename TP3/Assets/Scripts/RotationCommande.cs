using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RotationCommande : MonoBehaviour
{
    public GameObject Rotation;
    public string axe;
    public int Sens = 1;

    // Commande pour l'articulation de rotation
    // Pour tourner la flèche Positif : W - Négatif : X
    // Pour tourner  les supports Positif : E - Négatif : A
    // Pour déployer les supports au sol Positif : F - Négatif : R
    // Pour touner le mat Positif : V - Négatif : C

    void Update()// Renvoie l'état de mouvement à RotationControleur
    {
        float input = Input.GetAxis(axe);
        EtatRotation rotationState = MoveStateForInput(input);
        RotationControleur controller = Rotation.GetComponent<RotationControleur>();
        controller.rotationState = rotationState;
    }

    // Renvoie l'état dans lequel devrait être l'articulation de mouvement
    EtatRotation MoveStateForInput(float input)
    {
        if (Sens == 1)
        {
            if (input > 0)
            {
                return EtatRotation.Positif;
            }
            else if (input < 0)
            {
                return EtatRotation.Negatif;
            }
            else
            {
                return EtatRotation.Fixe;
            }
        }
        else if (Sens == -1)
        {
            if (input < 0)
            {
                return EtatRotation.Positif;
            }
            else if (input > 0)
            {
                return EtatRotation.Negatif;
            }
            else
            {
                return EtatRotation.Fixe;
            }
        }
        else
        {
            return EtatRotation.Fixe;
        }
    }
}
