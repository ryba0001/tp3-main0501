using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Corde : MonoBehaviour
{
    public Transform crochet; // Référence au crochet
    public Transform moufle;  // Référence à la moufle
    private LineRenderer lineRenderer;

    // Start is called before the first frame update
    void Start()
    {
        lineRenderer = gameObject.AddComponent<LineRenderer>();
        lineRenderer.startWidth = 0.05f;
        lineRenderer.endWidth = 0.05f;
        lineRenderer.material = new Material(Shader.Find("Sprites/Default"));
        lineRenderer.positionCount = 2;
        lineRenderer.startColor = Color.red; // Met le début de la corde en rouge
        lineRenderer.endColor = Color.red;   // Met la fin de la corde en rouge
    }

    // Update is called once per frame
    void Update()
    {
        // Mettre à jour les positions de la ligne
        lineRenderer.SetPosition(0, crochet.position); // Position du crochet
        lineRenderer.SetPosition(1, moufle.position);  // Position de la moufle
    }
}