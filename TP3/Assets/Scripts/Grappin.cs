using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Grappin : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKey(KeyCode.Space))
        {
            Destroy(this.gameObject.GetComponent<FixedJoint>());
        }

    }

    // Permet de créer une connexion fixe entre l'objet actuel et tout objet avec lequel il entre en collision
    void OnCollisionEnter(Collision Collision)
    {
        if (Collision.gameObject.CompareTag("Crochet"))
        {
            FixedJoint joint = this.gameObject.AddComponent<FixedJoint>();
            joint.connectedArticulationBody = Collision.articulationBody;
        }
    }
}
